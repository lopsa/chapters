# LOPSA Chapter Handbook: Organizing and Running a LOPSA Chapter

**Intended audience**: now and future LOPSA chapter organizers and volunteers.

**Editor**: [Aleksey Tsalolikhin](mailto:atsaloli@lopsa.org), Member of the LOPSA board of directors and organizer of the LOPSA L.A. chapter

**Purpose**: To document a workable pattern for starting and running a LOPSA chapter in order to make it easier for LOPSA members to start and operate thriving LOPSA chapters that add value to the local LOPSA membership and to their community.

Feedback and merge requests welcome!

## Establishing your chapter

### Membership requirements

Memberhip requirements (minimum number LOPSA members
needed to form a chapter) are detailed in 
[Article 3.1 Regular Chapters](http://governance.lopsa.org/LOPSA_Policies/Locals_program#Regular_Chapter)
of [LOPSA Locals program](http://governance.lopsa.org/LOPSA_Policies/Locals_program).

### Name

For new groups, we recommend the pattern "LOPSA" + place name, e.g.:
- LOPSA Columbus
- LOPSA East Tennesse
- LOPSA L.A.

Existing groups meeting membership requirements are of course welcome
to register as LOPSA chapters under existing names, e.g.:
- Ottawa Valley Sage
- Seattle Area System Administrators Guild (SASAG)

### Application process

The application process is detailed in http://governance.lopsa.org/LOPSA_Policies/Locals_program#Regular_Chapter_Application

Email [board@lopsa.org](mailto:board@lopsa.org) to apply.

Email [Aleksey Tsalolikhin](mailto:atsaloli@lopsa.org) if you need any help with your application.

### Web site
You can set up a web presence through a meetup/event service, such as meetup.com or eventbrite.com, as have:
- LOPSA LA https://www.meetup.com/lopsala/
- LOPSA Columbus http://lopsacbus.org/

Or you can make your own web site, as have:
- Ottawa Valley Sage [https://ovsage.org/](https://ovsage.org/)
- East Tennessee chapter [http://lopsaetenn.org](http://web.lopsaetenn.org)
- Seattle Area System Administrators Guild (SASAG) [https://sasag.org](https://sasag.org)

### Finding venues

There are often companies willing to host local meetups in exchange
for an opportunity to solicit job application, to promote their services
or for goodwill.

We have also had chapter organizer employers host meetups in this fashion.

We have also had chapters find meeting space at a community college (through a
student affiliate organization) and at a university (where the chapter leaders
were employed).

LOPSA L.A. also does occasional "social" dinner meetups at local restaurants.

### Finding speakers

Network with your local community to find speakers.



## Running a meetup

### Before the meetup
- Schedule the meetup on your website
- Announce the meetup on your web site
- Announce the meetup through social media
- Email [LOPSA Community Manager](rluedecke@lopsa.org) to add your event to the events list on www.LOPSA.org and to list it in the next LOPSAGram.
- Organize sufficient food and drinks for the attendees (it's nice to have a corporate
sponsor for that -- sometimes the host provides it but not always).

## During your meetup

- Have adequate signage so attendees can easily find you.
- Greet attendees, and introduce them to each other as needed (to make sure no one is left standing alone in a corner).
- Allow the first half an hour for socializing over food/drinks.
- Make sure the speaker is doing OK and no technical issues (can project OK if presenting).

- Half an hour after the scheduled start time, end the social time and start the presentation section of the meetup (i.e., invite people to be seated)
- Introduce the parent organization, LOPSA.org
- Announce the LOPSA mentorship program -- LOPSA members can sign up as a protege or mentor at [mentor.lopsa.org](https://mentor.lopsa.org)
- Ask who in the room is a LOPSA member and thank them for their membership.
- Tell the rest how they can [join](https://lopsa.org/Join-or-Renew)
- Thank the host and let them speak for a couple of minutes if they want - make sure they get a nice acknowledgement from the group.
- Ask who is looking for work?
- Ask who is hiring?
- Ask for any community announcements?
- Ask for help running the meetup
- Call for speakers
- Take a picture of your meetup and send it to [LOPSA Community Manager](rluedecke@lopsa.org) for the next LOPSAGram.
- Introduce the speaker and turn the floor over to them.
- After the presentation, make sure the speaker gets a nice round of applause.
- Thank the speaker for presenting
- Get everybody out of the room on time so the host can close up shop.
